import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Event } from './entity/event.entity';
import { EventStoreService } from './event-store.service';
import { EventStoreListener } from './event-store.listener';
import { EventSourcingController } from './event-sourcing.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { GetAllEventsHandler } from './query/handler/get-all-events.handler';
import { ConfigModule } from '../config/config.module';

@Module({
    imports: [ConfigModule, TypeOrmModule.forFeature([Event]), CqrsModule],
    exports: [TypeOrmModule, EventStoreService],
    providers: [EventStoreService, EventStoreListener, GetAllEventsHandler],
    controllers: [EventSourcingController],
})
export class EventSourcingModule {}
