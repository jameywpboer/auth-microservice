import { PrimaryColumn, Column, Entity } from 'typeorm';

@Entity()
export class Event {
    @PrimaryColumn()
    id: number;

    @Column({ length: 50 })
    type: string;

    @Column({ length: 50 })
    aggregateId: string;

    @Column('json')
    data: JSON;

    @Column('datetime')
    created: Date;

    @Column('bit')
    published: boolean;
}
