import { Injectable, Scope } from '@nestjs/common';
import { BaseEvent } from './event/base.event';
import { ConfigService } from '../config/config.service';
const es = require('eventstore');

@Injectable()
export class EventStoreService {
    private readonly eventStore;
    constructor(private readonly config: ConfigService) {
        this.eventStore = es({
            type: config.get('ES_TYPE'),
            host: config.get('ES_HOST'),
            port: config.get('ES_PORT'),
            username: config.get('ES_USERNAME'),
            password: config.get('ES_PASSWORD'),
        });

        this.eventStore.init(err => {
            if (err) return console.error(err);
        });
    }

    public publishEvent({ aggregateId, aggregate, context, data }: BaseEvent) {
        this.eventStore.getEventStream(
            {
                aggregateId: aggregateId,
                aggregate: aggregate,
                context: context,
            },
            (err, stream) => {
                if (err) return console.log(err);
                stream.addEvent(data);
                stream.commit();
            },
        );
    }

    public async getAllEvents() {
        return new Promise((resolve, reject) => {
            this.eventStore.getEvents(0, -1, (err, events) => {
                if (err) return reject(err);
                resolve(events);
            });
        });
    }
}
