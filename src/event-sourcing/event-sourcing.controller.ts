import { Controller } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { MessagePattern } from '@nestjs/microservices';
import { GetAllEventsQuery } from './query/get-all-events.query';

@Controller('event-sourcing')
export class EventSourcingController {
    constructor(private readonly queryBus: QueryBus) {}

    @MessagePattern('GetAllEvents')
    async GetAllEvents() {
        return this.queryBus.execute(new GetAllEventsQuery());
    }
}
