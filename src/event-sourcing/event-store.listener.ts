import { Injectable } from '@nestjs/common';
import { Saga } from '@nestjs/cqrs';
import { map, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EventStoreService } from './event-store.service';

@Injectable()
export class EventStoreListener {
    constructor(private readonly eventStoreService: EventStoreService) {}

    @Saga()
    events = (events$: Observable<any>) => {
        return events$.pipe(
            map(event => {
                this.eventStoreService.publishEvent(event);
                return null;
            }),
        );
    };
}
