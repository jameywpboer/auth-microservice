import { IEvent } from '@nestjs/cqrs';

export class BaseEvent implements IEvent {
    public readonly aggregate: string;
    public readonly context: string;

    constructor(
        public readonly aggregateId: string,
        public readonly data: any,
    ) {}
}
