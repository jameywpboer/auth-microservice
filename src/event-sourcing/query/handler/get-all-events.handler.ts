import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetAllEventsQuery } from '../get-all-events.query';
import { EventStoreService } from '../../event-store.service';

@QueryHandler(GetAllEventsQuery)
export class GetAllEventsHandler implements IQueryHandler<GetAllEventsQuery> {
    constructor(private readonly eventStoreService: EventStoreService) {}

    async execute(query: GetAllEventsQuery) {
        return this.eventStoreService.getAllEvents();
    }
}
