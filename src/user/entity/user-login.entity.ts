import { PrimaryColumn, Column, Entity } from 'typeorm';

@Entity()
export class UserLogin {
    @PrimaryColumn()
    userId: string;

    @Column({ length: 50 })
    email: string;

    @Column({ length: 50 })
    password: string;

    @Column('datetime')
    lastLogin: Date;

    @Column({ length: 50 })
    lastLoginMethod: string;

    @Column('int')
    failedLogins: number;

    @Column('datetime')
    lastFailedLogin: Date;
}
