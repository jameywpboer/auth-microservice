import { AggregateRoot, IEvent } from '@nestjs/cqrs';
import { UserLoggedInEvent } from '../authentication/event/user-logged-in.event';
import { UserLoginFailedEvent } from '../authentication/event/user-login-failed.event';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { UserLogin } from './entity/user-login.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserModel extends AggregateRoot {
    public aggregateId: string;

    constructor(
        @InjectRepository(UserLogin)
        private readonly repository: Repository<UserLogin>,
    ) {
        super();
        this.autoCommit = false;
    }

    // REPOSITORY STUFF
    async findByEmail(email: string): Promise<UserLogin> {
        return this.repository.findOne({ where: { email } });
    }

    userLogin(type: string) {
        console.log('UserLogin');
        this.apply(new UserLoggedInEvent(this.aggregateId, { type }));
    }

    userLoginFail(password: string) {
        console.log('UserLoginFail');
        this.apply(new UserLoginFailedEvent(this.aggregateId, { password }));
    }
}
