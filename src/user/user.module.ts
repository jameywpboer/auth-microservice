import { Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { UserLogin } from './entity/user-login.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModel } from './user.model';
import { EventSourcingModule } from '../event-sourcing/event-sourcing.module';

@Module({
    imports: [
        ConfigModule,
        EventSourcingModule,
        TypeOrmModule.forFeature([UserLogin]),
    ],
    providers: [UserModel],
    exports: [TypeOrmModule, UserModel],
})
export class UserModule {}
