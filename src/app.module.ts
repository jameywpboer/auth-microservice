import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AuthenticationModule } from './authentication/authentication.module';
import { ConfigModule } from './config/config.module';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './config/config.service';
import { Connection } from 'typeorm';
import { EventSourcingModule } from './event-sourcing/event-sourcing.module';

const configService = new ConfigService(
    `${process.env.NODE_ENV || 'development'}.env`,
);

@Module({
    imports: [
        EventSourcingModule,
        AuthenticationModule,
        ConfigModule,
        UserModule,
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: configService.get('DB_HOST'),
            port: Number(configService.get('DB_PORT')),
            username: configService.get('DB_USERNAME'),
            password: configService.get('DB_PASSWORD'),
            database: configService.get('DB_DATABASE'),
            entities: [__dirname + '/**/entity/*.entity{.ts,.js}'],
            synchronize: false,
        }),
    ],
    controllers: [AppController],
})
export class AppModule {
    constructor(private readonly connection: Connection) {}
}
