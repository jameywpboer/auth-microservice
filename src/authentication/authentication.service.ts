import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserLoginDto } from './dto/user-login.dto';

@Injectable()
export class AuthenticationService {
    constructor(private readonly jwtService: JwtService) {}

    validateUser(email: string, password: string) {
        return {
            id: 1,
            email: email,
            password: password,
        };
    }

    async createJwtToken(userInformation) {
        return this.jwtService.sign(userInformation);
    }
}
