import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { DoUserValidateCommand } from '../do-user-validate.command';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { UserLogin } from '../../../user/entity/user-login.entity';
import { InjectRepository } from '@nestjs/typeorm';

@CommandHandler(DoUserValidateCommand)
export class DoUserValidateHandler
    implements ICommandHandler<DoUserValidateCommand> {
    constructor(private readonly jwtService: JwtService) {}

    async execute(command: DoUserValidateCommand) {
        const { token } = command;

        console.log('Validate');

        let success = false;
        try {
            this.jwtService.verify(token);
            success = true;
        } catch (e) {
            success = false;
        }

        const userId = this.jwtService.decode(token)['id'];

        return { success, userId };
    }
}
