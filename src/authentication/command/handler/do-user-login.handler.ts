import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { DoUserLoginCommand } from '../do-user-login.command';
import { JwtService } from '@nestjs/jwt';
import { UserModel } from '../../../user/user.model';

@CommandHandler(DoUserLoginCommand)
export class DoUserLoginHandler implements ICommandHandler<DoUserLoginCommand> {
    constructor(
        private readonly publisher: EventPublisher,
        private readonly jwtService: JwtService,
        private readonly user: UserModel,
    ) {}

    async execute(command: DoUserLoginCommand) {
        const { email, password } = command;
        let userEntity = await this.user.findByEmail(email);

        if (!userEntity) return;
        this.user.aggregateId = userEntity.userId;

        const mergedModel = this.publisher.mergeObjectContext(this.user);

        if (userEntity.password !== password) {
            mergedModel.userLoginFail(password);
            mergedModel.commit();
            return {
                success: false,
                userId: null,
                token: null,
            };
        }

        mergedModel.userLogin('email-password');
        mergedModel.commit();

        return {
            success: true,
            userId: userEntity.userId,
            token: this.jwtService.sign({
                id: userEntity.userId,
                email: userEntity.email,
                password: userEntity.password,
            }),
        };
    }
}
