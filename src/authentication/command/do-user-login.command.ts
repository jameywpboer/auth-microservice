export class DoUserLoginCommand {
    constructor(
        public readonly email: string,
        public readonly password: string,
    ) {}
}
