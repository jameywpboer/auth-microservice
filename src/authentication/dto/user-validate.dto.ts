import { IsNotEmpty } from 'class-validator';

export class UserValidateDto {
    @IsNotEmpty()
    readonly token: string;
}
