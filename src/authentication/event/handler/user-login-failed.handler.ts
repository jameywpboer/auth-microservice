import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserLoggedInEvent } from '../user-logged-in.event';
import { UserLoginFailedEvent } from '../user-login-failed.event';
import { Repository, EntityManager } from 'typeorm';
import { UserLogin } from '../../../user/entity/user-login.entity';
import { InjectRepository } from '@nestjs/typeorm';

@EventsHandler(UserLoggedInEvent)
export class UserLoginFailedHandler
    implements IEventHandler<UserLoginFailedEvent> {
    constructor(
        @InjectRepository(UserLogin)
        private readonly repository: Repository<UserLogin>,
        private readonly entityManager: EntityManager,
    ) {}

    async handle(event: UserLoginFailedEvent) {
        const userLogin = await this.repository.findOne(event.aggregateId);
        userLogin.failedLogins++;
        userLogin.lastFailedLogin = new Date();
        this.entityManager.save(userLogin);
    }
}
