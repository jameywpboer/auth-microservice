import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserLoggedInEvent } from '../user-logged-in.event';
import { Repository, EntityManager } from 'typeorm';
import { UserLogin } from '../../../user/entity/user-login.entity';
import { InjectRepository } from '@nestjs/typeorm';

@EventsHandler(UserLoggedInEvent)
export class UserLoggedInHandler implements IEventHandler<UserLoggedInEvent> {
    constructor(
        @InjectRepository(UserLogin)
        private readonly repository: Repository<UserLogin>,
        private readonly entityManager: EntityManager,
    ) {}

    async handle(event: UserLoggedInEvent) {
        const userLogin = await this.repository.findOne(event.aggregateId);
        userLogin.lastLogin = new Date();
        userLogin.lastLoginMethod = event.data.type;
        this.entityManager.save(userLogin);
    }
}
