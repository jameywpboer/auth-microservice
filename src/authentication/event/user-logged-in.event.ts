import { BaseEvent } from '../../event-sourcing/event/base.event';

export class UserLoggedInEvent extends BaseEvent {
    public readonly aggregate: string = 'user';
    public readonly context: string = 'UserLoggedIn';
}
