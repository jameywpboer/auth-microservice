import { BaseEvent } from '../../event-sourcing/event/base.event';
import { IEvent } from '@nestjs/cqrs';

export class UserLoginFailedEvent extends BaseEvent implements IEvent {
    public readonly aggregate: string = 'user';
    public readonly context: string = 'UserLoginFailed';
}
