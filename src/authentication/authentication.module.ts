import { Module } from '@nestjs/common';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { CqrsModule } from '@nestjs/cqrs';
import { UserLoggedInHandler } from './event/handler/user-logged-in.handler';
import { UserLoginFailedHandler } from './event/handler/user-login-failed.handler';
import { DoUserLoginHandler } from './command/handler/do-user-login.handler';
import { DoUserValidateHandler } from './command/handler/do-user-validate.handler';
import { UserModule } from '../user/user.module';

@Module({
    imports: [
        CqrsModule,
        UserModule,
        ConfigModule,
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get('JWT_SECRET'),
                signOptions: {
                    expiresIn: configService.get('JWT_EXPIRE_DATE'),
                },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [AuthenticationController],
    providers: [
        AuthenticationService,
        JwtStrategy,
        UserLoggedInHandler,
        UserLoginFailedHandler,
        DoUserLoginHandler,
        DoUserValidateHandler,
    ],
})
export class AuthenticationModule {}
