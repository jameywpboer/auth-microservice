import { Controller, Body } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { UserLoginDto } from './dto/user-login.dto';
import { MessagePattern } from '@nestjs/microservices';
import { CommandBus, EventBus } from '@nestjs/cqrs';
import { DoUserLoginCommand } from './command/do-user-login.command';
import { DoUserValidateCommand } from './command/do-user-validate.command';
import { UserValidateDto } from './dto/user-validate.dto';

@Controller()
export class AuthenticationController {
    constructor(private readonly commandBus: CommandBus) {}

    @MessagePattern('DoUserLogin')
    async DoUserLogin(@Body() userLoginDto: UserLoginDto) {
        return this.commandBus.execute(
            new DoUserLoginCommand(userLoginDto.email, userLoginDto.password),
        );
    }

    @MessagePattern('DoUserValidate')
    async DoUserValidate(@Body() userValidateDto: UserValidateDto) {
        return this.commandBus.execute(
            new DoUserValidateCommand(userValidateDto.token),
        );
    }
}
