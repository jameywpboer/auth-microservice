import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { Transport } from '@nestjs/common/enums/transport.enum';

const logger = new Logger('Main');

async function bootstrap() {
    const app = await NestFactory.createMicroservice(AppModule, {
        transport: Transport.TCP,
        options: {
            host: '127.0.0.1',
            port: 6001,
        },
    });
    await app.listen(() =>
        logger.log('Microservice is listening on port 6001...'),
    );
}
bootstrap();
